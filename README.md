# "Bomberman" IKA test project
(c) 2018 Michał Siejak

Time spent: around 14h (definitely close to the time limit of 15h).
Bonus objectives completed: dynamic camera, procedural map.

Content in "ThirdParty" directory:

- UE4 Mannequin imported from third person template project.
- [SuperGrid](https://www.unrealengine.com/marketplace/supergrid-starter-pack) level prototyping assets.

.uasset files are kept in Git LFS.

## Controls

- 1st player: WASD for movement, space for bomb planting/remote detonation.
- 2nd player: Arrow keys for movement, enter for bomb planting/remote detonation.

Escape quits the game.

## Notes

I've found the enemy AI objective not feasable to complete in the 15h time constraint.
Creating an AI intelligent enough to be fun and engaging requires a lot of time & tweaking.
If I were to add it my plan would be to use behavior trees & EQS for AI logic. This would
also require refactoring of `ABombermanPlayerCharacter` into two classes: `ABombermanCharacter` &
`ABombermanPlayerCharacter` with the latter containing only player input code.
Procedural map generator would also need to build a navmesh for the level for AI controllers
to properly navigate.

If I were to continue working on the project my next steps would be:

- Implementation of enemy AI players
- Implementation of passive enemy creatures wandering around the level.
- Addition of a networked multiplayer mode.
- Implementation of an online subsystem for simple online leaderboards.
- Addition of different game modes: free for all, team deathmatch, etc.
- Addition of a single player campaign mode like in Dyna Blaster.
- Larger/more varied levels with different generators.
- Addition of proper main & option menus.

