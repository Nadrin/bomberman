// (c) 2018 Michal Siejak

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BombermanLevel.generated.h"

UCLASS()
class BOMBERMAN_API ABombermanLevel : public AActor
{
	GENERATED_BODY()
public:	
	ABombermanLevel();

	/* Begin AActor interface */
	virtual void BeginPlay() override;
	/* End AActor interface */

protected:
	UPROPERTY(EditAnywhere, Category="Bomberman|Level")
	int32 NumBlocksWidth;
	
	UPROPERTY(EditAnywhere, Category="Bomberman|Level")
	int32 NumBlocksHeight;
	
	UPROPERTY(EditAnywhere, Category="Bomberman|Level")
	TSubclassOf<AActor> WallClass;
	
	UPROPERTY(EditAnywhere, Category="Bomberman|Level")
	TSubclassOf<AActor> DestructibleWallClass;
	
	UPROPERTY(EditAnywhere, Category="Bomberman|Level")
	float StartLocationHeight;

	UPROPERTY(EditAnywhere, Category="Bomberman|Level")
	int32 PlayerStartMinWallDistance;
	
	UPROPERTY(EditAnywhere, Category="Bomberman|Level")
	float WallProbability;
	
	UPROPERTY(EditAnywhere, Category="Bomberman|Level")
	int32 Seed;

private:
	void CalcBlockDeltas();
	void GenerateLevel();
	void InitializePlayers();

	FVector GetWorldLocationAt(int32 BlockX, int32 BlockY) const;

	FRandomStream RandomStream;

	float BlockDeltaX;
	float BlockDeltaY;

	FIntVector PlayerOneStart;
	FIntVector PlayerTwoStart;
};
