// (c) 2018 Michal Siejak

#include "BombermanLevel.h"
#include "Core/BombermanGameMode.h"
#include "Player/BombermanPlayerCharacter.h"
#include "Bomberman.h"

#include "Engine/World.h"
#include "EngineUtils.h"

ABombermanLevel::ABombermanLevel()
	: NumBlocksWidth(11)
	, NumBlocksHeight(11)
	, StartLocationHeight(100.0f)
	, PlayerStartMinWallDistance(1)
	, WallProbability(0.5f)
{}

void ABombermanLevel::BeginPlay()
{
	Super::BeginPlay();

	if(!WallClass || !DestructibleWallClass)
	{
		UE_LOG(LogBomberman, Error, TEXT("Wall class configuration missing for level generator. Bailing out!"));
		return;
	}

	if(Seed == 0)
	{
		// Randomize seed if it's not set to a specific value.
		Seed = static_cast<int32>(FPlatformTime::Cycles());
	}
	RandomStream = FRandomStream{Seed};
	UE_LOG(LogBomberman, Log, TEXT("Generating level with seed = %d"), Seed);

	PlayerOneStart = FIntVector{1, 1, 0};
	PlayerTwoStart = FIntVector{FMath::Max(NumBlocksHeight-2, 1), FMath::Max(NumBlocksWidth-2, 1), 0};

	CalcBlockDeltas();
	GenerateLevel();
	InitializePlayers();
}
	
void ABombermanLevel::CalcBlockDeltas()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	// Unfortunately CDOs don't have bounds information, so we need to temporarily spawn a dummy wall
	// to retrieve its bounds.
	AActor* DummyWallActor = GetWorld()->SpawnActor<AActor>(WallClass, SpawnParams);
	if(DummyWallActor)
	{
		FVector Origin, Extent;
		DummyWallActor->GetActorBounds(true, Origin, Extent);
		BlockDeltaX = 2.0f * Extent.X;
		BlockDeltaY = 2.0f * Extent.Y;
		DummyWallActor->Destroy();
	}
}
	
void ABombermanLevel::GenerateLevel()
{
	// Calculate Chebyshev distance between two level blocks.
	auto GetDistance = [](int32 X1, int32 Y1, int32 X2, int32 Y2)
	{
		return FMath::Max(FMath::Abs(X2 - X1), FMath::Abs(Y2 - Y1));
	};

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	for(int32 X=0; X<NumBlocksHeight; ++X)
	{
		for(int32 Y=0; Y<NumBlocksWidth; ++Y)
		{
			const FVector WorldLocation = GetWorldLocationAt(X, Y);

			// Spawn indestructible wall (block) at
			// (a) level boundary
			// (b) every other free space in both horiz. and vertical directions
			if(X == 0 || Y == 0 || X == NumBlocksHeight-1 || Y == NumBlocksWidth-1 || (X % 2 == 0 && Y % 2 == 0))
			{
				GetWorld()->SpawnActor<AActor>(WallClass, WorldLocation, FRotator::ZeroRotator, SpawnParams);
			}
			// Otherwise possibly spawn a destructible wall (block) but only if we're sufficiently away from both players start points.
			else if(GetDistance(X, Y, PlayerOneStart.X, PlayerOneStart.Y) > PlayerStartMinWallDistance
				 && GetDistance(X, Y, PlayerTwoStart.X, PlayerTwoStart.Y) > PlayerStartMinWallDistance)
			{
				if(RandomStream.GetFraction() <= WallProbability)
				{
					GetWorld()->SpawnActor<AActor>(DestructibleWallClass, WorldLocation, FRotator::ZeroRotator, SpawnParams);
				}
			}
		}
	}
}

void ABombermanLevel::InitializePlayers()
{
	// So we want both players to spawn at correct level "tiles" depending on chosen level dimensions (NumBlocksWidth/NumBlocksHeight).
	// Unfortunately engine player spawning logic doesn't really play well with "dynamic" player starts (created after players have already "logged into" the game).
	// In this case it's much simpler to teleport pawns after the fact than to override AGameModeBase::ChosePlayerStart().

	const FVector StartLocations[] = {
		GetWorldLocationAt(PlayerOneStart.X, PlayerOneStart.Y) + FVector{0.0f, 0.0f, StartLocationHeight},
		GetWorldLocationAt(PlayerTwoStart.X, PlayerTwoStart.Y) + FVector{0.0f, 0.0f, StartLocationHeight},
	};
	const FVector StartRotations[] = {
		FVector{0.0f, 0.0f, 0.0f},
		FVector{0.0f, 0.0f, 180.0f},
	};

	int32 CharacterIndex = 0;
	for(TActorIterator<ACharacter> It{GetWorld()}; It; ++It)
	{
		FTransform StartTransform;
		StartTransform.SetLocation(StartLocations[CharacterIndex % 2]);
		StartTransform.SetRotation(FQuat::MakeFromEuler(StartRotations[CharacterIndex % 2]));
		(*It)->SetActorTransform(StartTransform);
		++CharacterIndex;
	}
}
	
FVector ABombermanLevel::GetWorldLocationAt(int32 BlockX, int32 BlockY) const
{
	const FVector BottomLeftOrigin = {-0.5f * NumBlocksHeight * BlockDeltaX, -0.5f * NumBlocksWidth * BlockDeltaY, 0.0f};
	return FVector{BottomLeftOrigin.X + BlockX * BlockDeltaX, BottomLeftOrigin.Y + BlockY * BlockDeltaY, 0.0f};
}
