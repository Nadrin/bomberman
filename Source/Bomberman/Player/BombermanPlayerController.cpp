// (c) 2018 Michal Siejak

#include "BombermanPlayerController.h"
#include "BombermanPlayerCharacter.h"
#include "Core/BombermanGameMode.h"
#include "Bomberman.h"

#include "Engine.h"
#include "Engine/World.h"
#include "Engine/LocalPlayer.h"
	
void ABombermanPlayerController::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);
	if(ABombermanPlayerCharacter* Character = Cast<ABombermanPlayerCharacter>(InPawn))
	{
		Character->OnDied.AddDynamic(this, &ABombermanPlayerController::OnPlayerDied);
	}
}

bool ABombermanPlayerController::InputKey(FKey Key, EInputEvent EventType, float AmountDepressed, bool bGamepad)
{
	bool bAccepted = false;

	bAccepted |= Super::InputKey(Key, EventType, AmountDepressed, bGamepad);
	if(bForwardInputToNextPlayerController)
	{
		const int32 NextControllerId = GetThisControllerId() + 1;
		ULocalPlayer* NextLocalPlayer = GEngine->GetLocalPlayerFromControllerId(GetWorld(), NextControllerId);
		if(NextLocalPlayer)
		{
			bAccepted |= NextLocalPlayer->PlayerController->InputKey(Key, EventType, AmountDepressed, bGamepad);
		}
	}
	return bAccepted;
}
	
void ABombermanPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if(GetThisControllerId() == 0)
	{
		InputComponent->BindAction("RestartGame", IE_Pressed, this, &ABombermanPlayerController::Action_RestartGame)
			.bExecuteWhenPaused = true;
		InputComponent->BindAction("QuitGame", IE_Pressed, this, &ABombermanPlayerController::Action_QuitGame)
			.bExecuteWhenPaused = true;
	}
}
	
int32 ABombermanPlayerController::GetThisControllerId() const
{
	const ULocalPlayer* ThisLocalPlayer = GetLocalPlayer();
	check(ThisLocalPlayer);
	return ThisLocalPlayer->GetControllerId();
}

void ABombermanPlayerController::OnPlayerDied(ABombermanPlayerCharacter* InCharacter)
{
	GetWorld()->GetAuthGameMode<ABombermanGameMode>()->NotifyPlayerDied(GetThisControllerId());
}

void ABombermanPlayerController::Action_RestartGame()
{
	AGameMode* GameMode = GetWorld()->GetAuthGameMode<AGameMode>();
	check(GameMode);

	if(GameMode->GetMatchState() == MatchState::WaitingPostMatch)
	{
		GameMode->RestartGame();
	}
}
	
void ABombermanPlayerController::Action_QuitGame()
{
	FPlatformMisc::RequestExit(false);
}
