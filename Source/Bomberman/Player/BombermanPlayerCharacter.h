// (c) 2018 Michał Siejak

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Common/BombermanEntityInterface.h"

#include "BombermanPlayerCharacter.generated.h"

UCLASS(abstract)
class BOMBERMAN_API ABombermanPlayerCharacter : public ACharacter
	                                          , public IBombermanEntityInterface
{
	GENERATED_BODY()
public:
	ABombermanPlayerCharacter(const FObjectInitializer& ObjectInitializer);

	/* Begin APawn interface */
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	/* End APawnInterface */

	/* Begin IBombermanEntityInterface */
	virtual void DestroyEntity_Implementation() override;
	/* End IBombermanEntityInterace */

	UFUNCTION(BlueprintCallable, Category="Bomberman")
	void PlantBomb();
	
	UFUNCTION(BlueprintCallable, Category="Bomberman")
	void DetonateRemoteBombs();

	UFUNCTION(BlueprintCallable, Category="Bomberman|Statistics")
	void EnableRemoteControl(float Duration);

	UFUNCTION(BlueprintCallable, Category="Bomberman|Statistics")
	void DisableRemoteControl();

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDiedDelegate, ABombermanPlayerCharacter*, Character);
	UPROPERTY(BlueprintAssignable, Category="Bomberman|Events")
	FDiedDelegate OnDied;

protected:
	UPROPERTY(EditDefaultsOnly, Category=Bomberman)
	TSubclassOf<class ABombermanBomb> BombClass;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Bomberman|Statistics")
	float BlastRadius;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Bomberman|Statistics")
	int32 BombsRemaining;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Bomberman|Statistics")
	uint32 bHasRemoteControl:1;

	UPROPERTY(BlueprintReadOnly, Category="Bomberman|State")
	TArray<class ABombermanBomb*> RemoteBombs;

private:
	void Input_MoveUp(float AxisValue);
	void Input_MoveRight(float AxisValue);
	void Action_PlantBomb();

	UFUNCTION()
	void OnBombDestroyed(AActor* InBomb);

	FTimerHandle RemoteControlTimerHandle;
};
