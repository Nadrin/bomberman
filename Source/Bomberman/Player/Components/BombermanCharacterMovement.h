// (c) 2018 Michal Siejak

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "BombermanCharacterMovement.generated.h"

UCLASS()
class BOMBERMAN_API UBombermanCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
public:
	UBombermanCharacterMovementComponent();

	/* Begin UMovementComponent interface */
	virtual float GetMaxSpeed() const override;
	/* End UMovementComponent interface */

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=BombermanMovement)
	float MovementSpeedMultiplier;
};
