// (c) 2018 Michal Siejak

#include "BombermanCharacterMovement.h"

	
UBombermanCharacterMovementComponent::UBombermanCharacterMovementComponent()
	: MovementSpeedMultiplier(1.0f)
{}

float UBombermanCharacterMovementComponent::GetMaxSpeed() const
{
	return MovementSpeedMultiplier * Super::GetMaxSpeed();
}
