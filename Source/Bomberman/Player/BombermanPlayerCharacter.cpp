// (c) 2018 Michał Siejak

#include "BombermanPlayerCharacter.h"
#include "BombermanPlayerController.h"
#include "Bomberman.h"

#include "Components/BombermanCharacterMovement.h"
#include "Items/BombermanBomb.h"

#include "Components/SkeletalMeshComponent.h"
#include "TimerManager.h"
	
ABombermanPlayerCharacter::ABombermanPlayerCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UBombermanCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
	, BlastRadius(1.0f)
	, BombsRemaining(1)
{
	PrimaryActorTick.bCanEverTick = true;
}
	
void ABombermanPlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	const ABombermanPlayerController* PlayerController = CastChecked<ABombermanPlayerController>(Controller);
	const int32 ControllerId = PlayerController->GetThisControllerId();

	auto GetBindingName = [ControllerId](const TCHAR* InName)
	{
		return FName{*FString::Printf(TEXT("%s_P%d"), InName, ControllerId+1)};
	};

	PlayerInputComponent->BindAxis(GetBindingName(TEXT("MoveUp")), this, &ABombermanPlayerCharacter::Input_MoveUp);
	PlayerInputComponent->BindAxis(GetBindingName(TEXT("MoveRight")), this, &ABombermanPlayerCharacter::Input_MoveRight);
	PlayerInputComponent->BindAction(GetBindingName(TEXT("PlantBomb")), IE_Pressed, this, &ABombermanPlayerCharacter::Action_PlantBomb);
}

void ABombermanPlayerCharacter::DestroyEntity_Implementation()
{
	if(APlayerController* PlayerController = Cast<APlayerController>(GetController()))
	{
		DisableInput(PlayerController);
	}

	GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	GetMesh()->SetSimulatePhysics(true);
	OnDied.Broadcast(this);
}

void ABombermanPlayerCharacter::PlantBomb()
{
	if(!BombClass)
	{
		UE_LOG(LogBomberman, Warning, TEXT("No bomb class has been configured for player character to use"));
		return;
	}
	if(BombsRemaining > 0)
	{
		ABombermanBomb* Bomb = ABombermanBomb::PlantBomb(this, BombClass, BlastRadius, bHasRemoteControl);
		if(Bomb)
		{
			--BombsRemaining;
			Bomb->OnDestroyed.AddDynamic(this, &ABombermanPlayerCharacter::OnBombDestroyed);
			if(bHasRemoteControl)
			{
				RemoteBombs.Add(Bomb);
			}
		}
	}
}
	
void ABombermanPlayerCharacter::DetonateRemoteBombs()
{
	// We need to take a copy because array might change during iteration.
	const TArray<ABombermanBomb*> ImmutableRemoteBombs = RemoteBombs;

	for(ABombermanBomb* Bomb : ImmutableRemoteBombs)
	{
		if(Bomb->IsArmed())
		{
			IBombermanEntityInterface::Execute_DestroyEntity(Bomb);
		}
	}
}
	
void ABombermanPlayerCharacter::EnableRemoteControl(float Duration)
{
	GetWorldTimerManager().SetTimer(RemoteControlTimerHandle, this, &ABombermanPlayerCharacter::DisableRemoteControl, Duration);
	bHasRemoteControl = true;
}
	
void ABombermanPlayerCharacter::DisableRemoteControl()
{
	GetWorldTimerManager().ClearTimer(RemoteControlTimerHandle);
	bHasRemoteControl = false;
}

void ABombermanPlayerCharacter::Input_MoveUp(float AxisValue)
{
	AddMovementInput(FVector{1.0f, 0.0f, 0.0f}, AxisValue);
}

void ABombermanPlayerCharacter::Input_MoveRight(float AxisValue)
{
	AddMovementInput(FVector{0.0f, 1.0f, 0.0f}, AxisValue);
}

void ABombermanPlayerCharacter::Action_PlantBomb()
{
	if(RemoteBombs.Num() > 0)
	{
		DetonateRemoteBombs();
	}
	else if(BombsRemaining > 0)
	{
		PlantBomb();
	}
}

void ABombermanPlayerCharacter::OnBombDestroyed(AActor* InBomb)
{
	ABombermanBomb* Bomb = CastChecked<ABombermanBomb>(InBomb);
	++BombsRemaining;
	RemoteBombs.Remove(Bomb);
}
