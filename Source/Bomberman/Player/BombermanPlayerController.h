// (c) 2018 Michal Siejak

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "Core/BombermanGameMode.h"

#include "BombermanPlayerController.generated.h"

class APawn;
class ABombermanPlayerCharacter;

UCLASS()
class BOMBERMAN_API ABombermanPlayerController : public APlayerController
{
	GENERATED_BODY()
public:	
	/* Begin AController interface */
	virtual void Possess(APawn* InPawn) override;
	/* End AController interface */

	/* Begin APlayerController interface */
	virtual bool InputKey(FKey Key, EInputEvent EventType, float AmountDepressed, bool bGamepad) override;
	virtual void SetupInputComponent() override;
	/* End APlayerController interface */
	
	int32 GetThisControllerId() const;

	UFUNCTION(BlueprintImplementableEvent, Category="Bomberman|UI")
	void ShowEndMatchScreen(EBombermanMatchResult MatchResult);

private:
	UFUNCTION()
	void OnPlayerDied(ABombermanPlayerCharacter* InCharacter);

	void Action_RestartGame();
	void Action_QuitGame();

	UPROPERTY(EditDefaultsOnly, Category=PlayerInput)
	uint32 bForwardInputToNextPlayerController:1;
};
