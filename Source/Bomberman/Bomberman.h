// (c) 2018 Michal Siejak

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogBomberman, Log, All);

// Custom collision channels.
#define ECC_Bomb  ECC_GameTraceChannel1
#define ECC_Wall  ECC_GameTraceChannel2
#define ECC_Blast ECC_GameTraceChannel3
