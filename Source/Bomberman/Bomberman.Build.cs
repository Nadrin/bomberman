// (c) 2018 Michal Siejak

using UnrealBuildTool;

public class Bomberman : ModuleRules
{
	public Bomberman(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore"
        });
	}
}
