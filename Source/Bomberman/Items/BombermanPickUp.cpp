// (c) 2018 Michal Siejak

#include "BombermanPickUp.h"
#include "Components/StaticMeshComponent.h"
#include "Player/BombermanPlayerCharacter.h"

ABombermanPickUp::ABombermanPickUp()
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetCollisionProfileName(TEXT("OverlapAllDynamic"));
	Mesh->OnComponentBeginOverlap.AddDynamic(this, &ABombermanPickUp::OnOverlap);
	Mesh->SetupAttachment(RootComponent);
}
	
void ABombermanPickUp::OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ABombermanPlayerCharacter* PlayerCharacter = Cast<ABombermanPlayerCharacter>(OtherActor);
	if(PlayerCharacter)
	{
		RecievePickedUp(PlayerCharacter);
	}
}
