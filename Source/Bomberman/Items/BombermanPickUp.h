// (c) 2018 Michal Siejak

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Common/BombermanEntityInterface.h"

#include "BombermanPickUp.generated.h"

class ABombermanPlayerCharacter;

UCLASS(abstract)
class BOMBERMAN_API ABombermanPickUp : public AActor
	                                 , public IBombermanEntityInterface
{
	GENERATED_BODY()
public:	
	ABombermanPickUp();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category="Bomberman|PickUp")
	void RecievePickedUp(ABombermanPlayerCharacter* PlayerCharacter);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Bomberman")
	UStaticMeshComponent* Mesh;

private:
	UFUNCTION()
	void OnOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
