// (c) 2018 Michal Siejak

#include "BombermanBomb.h"
#include "Bomberman.h"

#include "Components/StaticMeshComponent.h"
#include "GameFramework/Pawn.h"
#include "Engine/World.h"
#include "TimerManager.h"

ABombermanBomb::ABombermanBomb()
	: Timeout(2.0f)
	, bArmed(false)
{
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->OnComponentEndOverlap.AddDynamic(this, &ABombermanBomb::OnEndOverlap);

	Mesh->SetCollisionObjectType(ECC_Bomb);
	Mesh->SetCollisionResponseToAllChannels(ECR_Overlap);
	Mesh->SetCollisionResponseToChannel(ECC_Blast, ECR_Ignore);
	Mesh->SetCollisionResponseToChannel(ECC_Bomb, ECR_Block);

	RootComponent = Mesh;
}

ABombermanBomb* ABombermanBomb::PlantBomb(APawn* InInstigator, TSubclassOf<ABombermanBomb> BombClass, float InBlastRadius, bool bRemote)
{
	check(InInstigator);
	UWorld* World = InInstigator->GetWorld();

	FTransform BombTransform;
	BombTransform.SetLocation(InInstigator->GetActorLocation());
	ABombermanBomb* Bomb = World->SpawnActorDeferred<ABombermanBomb>(BombClass, BombTransform, nullptr, InInstigator, ESpawnActorCollisionHandlingMethod::DontSpawnIfColliding);
	if(Bomb) 
	{
		Bomb->PlantedBy   = InInstigator;
		Bomb->BlastRadius = InBlastRadius;
		if(bRemote)
		{
			Bomb->Timeout = 0.0f;
		}
		Bomb->FinishSpawning(BombTransform);

		// FinishSpawning marks actor for immediate destruction if it's impossible to spawn, handle that case and return null gracefully.
		return Bomb->IsPendingKill() ? nullptr : Bomb;
	}
	return nullptr;
}
	
void ABombermanBomb::OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if(OtherActor->GetFName() == PlantedBy->GetFName())
	{
		if(Timeout > 0.0f)
		{
			FTimerHandle TimeoutHandle;
			GetWorldTimerManager().SetTimer(TimeoutHandle, this, &ABombermanBomb::OnTimeout, Timeout);
		}
		Mesh->SetCollisionResponseToAllChannels(ECR_Block);

		bArmed = true;
		RecieveArmed();
	}
}

void ABombermanBomb::OnTimeout()
{
	IBombermanEntityInterface::Execute_DestroyEntity(this);
}
