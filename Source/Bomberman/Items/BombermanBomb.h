// (c) 2018 Michal Siejak

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"

#include "Common/BombermanEntityInterface.h"

#include "BombermanBomb.generated.h"

UCLASS(abstract)
class BOMBERMAN_API ABombermanBomb : public AActor
	                               , public IBombermanEntityInterface
{
	GENERATED_BODY()
public:	
	ABombermanBomb();

	UFUNCTION(BlueprintCallable, Category="Bomberman|Bomb")
	static ABombermanBomb* PlantBomb(APawn* InInstigator, TSubclassOf<ABombermanBomb> BombClass, float InBlastRadius=1.0f, bool bRemote=false);

	UFUNCTION(BlueprintCallable, Category="Bomberman|Bomb")
	bool IsArmed() const
	{
		return bArmed;
	}

protected:
	UFUNCTION(BlueprintImplementableEvent, Category="Bomberman|Bomb")
	void RecieveArmed();

	UPROPERTY(EditDefaultsOnly, Category="Bomberman|Bomb")
	float Timeout;
	
	UPROPERTY(BlueprintReadOnly, Category="Bomberman|Bomb")
	float BlastRadius;

	UPROPERTY(BlueprintReadOnly, Category="Bomberman|Bomb")
	AActor* PlantedBy;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Bomberman)
	UStaticMeshComponent* Mesh;

private:
	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	void OnTimeout();

	bool bArmed;
};
