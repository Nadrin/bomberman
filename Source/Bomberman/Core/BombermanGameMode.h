// (c) 2018 Michał Siejak

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "BombermanGameMode.generated.h"

UENUM(BlueprintType)
enum class EBombermanMatchResult : uint8
{
	Draw,
	PlayerOneWon,
	PlayerTwoWon,
};

UCLASS()
class BOMBERMAN_API ABombermanGameMode : public AGameMode
{
	GENERATED_BODY()
public:
	ABombermanGameMode();

	/* Begin AActor interface */
	virtual void BeginPlay() override;
	/* End AActor interface */
	
	/* Begin AGameMode interface */
	virtual void HandleMatchHasEnded() override;
	/* End AGameMode interface */
	
	void NotifyPlayerDied(int32 ControllerId);

	UFUNCTION(BlueprintCallable, Category="Bomberman|Game")
	float GetMatchTimeLeft() const;

	UPROPERTY(EditDefaultsOnly, Category="Bomberman|Game")
	float MatchDuration;

	UPROPERTY(EditDefaultsOnly, Category="Bomberman|Game")
	float PlayerDeathGracePeriod;

private:
	void ResolveMatch();

	FTimerHandle PlayerDeathTimerHandle;
	FTimerHandle MatchTimerHandle;

	bool bHasPlayerDied[2];
	EBombermanMatchResult MatchResult;
};
