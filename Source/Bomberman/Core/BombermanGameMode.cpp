// (c) 2018 Michał Siejak

#include "BombermanGameMode.h"
#include "BombermanGameInstance.h"
#include "Player/BombermanPlayerController.h"
#include "Bomberman.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "TimerManager.h"

ABombermanGameMode::ABombermanGameMode()
	: MatchDuration(60.0f)
	, PlayerDeathGracePeriod(5.0f)
	, MatchResult(EBombermanMatchResult::Draw)
{
}
	
void ABombermanGameMode::BeginPlay()
{
	Super::BeginPlay();

	if(NumPlayers < 2)
	{
		if(!UGameplayStatics::CreatePlayer(this))
		{
			UE_LOG(LogBomberman, Error, TEXT("Failed to spawn second player"));
		}
	}

	GetWorldTimerManager().SetTimer(MatchTimerHandle, this, &ABombermanGameMode::ResolveMatch, MatchDuration);
}
	
void ABombermanGameMode::HandleMatchHasEnded()
{
	Super::HandleMatchHasEnded();

	ABombermanPlayerController* PlayerController = CastChecked<ABombermanPlayerController>(GetGameInstance()->GetFirstLocalPlayerController(GetWorld()));
	PlayerController->SetPause(true);
	PlayerController->ShowEndMatchScreen(MatchResult);
}

float ABombermanGameMode::GetMatchTimeLeft() const
{
	return FMath::Max(0.0f, MatchDuration - GetWorld()->GetTimeSeconds());
}

void ABombermanGameMode::NotifyPlayerDied(int32 ControllerId)
{
	check(ControllerId >= 0 && ControllerId < 2);
	bHasPlayerDied[ControllerId] = true;

	GetWorldTimerManager().ClearTimer(MatchTimerHandle);
	GetWorldTimerManager().SetTimer(PlayerDeathTimerHandle, this, &ABombermanGameMode::ResolveMatch, PlayerDeathGracePeriod);
}

void ABombermanGameMode::ResolveMatch()
{
	GetWorldTimerManager().ClearAllTimersForObject(this);

	if(bHasPlayerDied[0] == bHasPlayerDied[1])
	{
		MatchResult = EBombermanMatchResult::Draw;
	}
	else
	{
		UBombermanGameInstance* GameInstance = CastChecked<UBombermanGameInstance>(GetGameInstance());
		if(bHasPlayerDied[0])
		{
			GameInstance->PlayerTwoScore++;
			MatchResult = EBombermanMatchResult::PlayerTwoWon;
		}
		else /* bHasPlayerDied[1] */
		{
			GameInstance->PlayerOneScore++;
			MatchResult = EBombermanMatchResult::PlayerOneWon;
		}
	}

	EndMatch();
}
