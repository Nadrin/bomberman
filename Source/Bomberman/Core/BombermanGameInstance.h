// (c) 2018 Michal Siejak

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "BombermanGameInstance.generated.h"

UCLASS()
class BOMBERMAN_API UBombermanGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadOnly, Category="Bomberman|Score")
	int32 PlayerOneScore;
	
	UPROPERTY(BlueprintReadOnly, Category="Bomberman|Score")
	int32 PlayerTwoScore;
};
