// (c) 2018 Michal Siejak

#pragma once

#include "CoreMinimal.h"
#include "BombermanEntityInterface.generated.h"

UINTERFACE(MinimalAPI)
class UBombermanEntityInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * Interface implemented by anything that can die or be destroyed.
 */
class BOMBERMAN_API IBombermanEntityInterface
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category="Bomberman|Entity")
	void DestroyEntity();
	virtual void DestroyEntity_Implementation() {}
};
