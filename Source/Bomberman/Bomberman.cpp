// (c) 2018 Michal Siejak

#include "Bomberman.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, Bomberman, "Bomberman");
DEFINE_LOG_CATEGORY(LogBomberman);
