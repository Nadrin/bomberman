// (c) 2018 Michal Siejak

using UnrealBuildTool;
using System.Collections.Generic;

public class BombermanTarget : TargetRules
{
	public BombermanTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "Bomberman" } );
	}
}
