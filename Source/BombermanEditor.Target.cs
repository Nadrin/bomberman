// (c) 2018 Michal Siejak

using UnrealBuildTool;
using System.Collections.Generic;

public class BombermanEditorTarget : TargetRules
{
	public BombermanEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "Bomberman" } );
	}
}
